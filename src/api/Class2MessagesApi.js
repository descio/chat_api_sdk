/**
 * Chat API SDK
 * The SDK allows you to receive and send messages through your WhatsApp account. [Sign up now](https://app.chat-api.com/)  The Chat API is based on the WhatsApp WEB protocol and excludes the ban both when using libraries from mgp25 and the like. Despite this, your account can be banned by anti-spam system WhatsApp after several clicking the \"block\" button.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: sale@chat-api.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.4.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ForwardMessageRequest', 'model/Messages', 'model/SendContactRequest', 'model/SendFileRequest', 'model/SendLinkRequest', 'model/SendLocationRequest', 'model/SendMessageRequest', 'model/SendMessageStatus', 'model/SendPTTRequest', 'model/SendVCardRequest'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ForwardMessageRequest'), require('../model/Messages'), require('../model/SendContactRequest'), require('../model/SendFileRequest'), require('../model/SendLinkRequest'), require('../model/SendLocationRequest'), require('../model/SendMessageRequest'), require('../model/SendMessageStatus'), require('../model/SendPTTRequest'), require('../model/SendVCardRequest'));
  } else {
    // Browser globals (root is window)
    if (!root.ChatApiSdk) {
      root.ChatApiSdk = {};
    }
    root.ChatApiSdk.Class2MessagesApi = factory(root.ChatApiSdk.ApiClient, root.ChatApiSdk.ForwardMessageRequest, root.ChatApiSdk.Messages, root.ChatApiSdk.SendContactRequest, root.ChatApiSdk.SendFileRequest, root.ChatApiSdk.SendLinkRequest, root.ChatApiSdk.SendLocationRequest, root.ChatApiSdk.SendMessageRequest, root.ChatApiSdk.SendMessageStatus, root.ChatApiSdk.SendPTTRequest, root.ChatApiSdk.SendVCardRequest);
  }
}(this, function(ApiClient, ForwardMessageRequest, Messages, SendContactRequest, SendFileRequest, SendLinkRequest, SendLocationRequest, SendMessageRequest, SendMessageStatus, SendPTTRequest, SendVCardRequest) {
  'use strict';

  /**
   * Class2Messages service.
   * @module api/Class2MessagesApi
   * @version 1.0.0
   */

  /**
   * Constructs a new Class2MessagesApi. 
   * @alias module:api/Class2MessagesApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the forwardMessage operation.
     * @callback module:api/Class2MessagesApi~forwardMessageCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Forwarding messages to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/ForwardMessageRequest} forwardMessageRequest 
     * @param {module:api/Class2MessagesApi~forwardMessageCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.forwardMessage = function(forwardMessageRequest, callback) {
      var postBody = forwardMessageRequest;
      // verify the required parameter 'forwardMessageRequest' is set
      if (forwardMessageRequest === undefined || forwardMessageRequest === null) {
        throw new Error("Missing the required parameter 'forwardMessageRequest' when calling forwardMessage");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/forwardMessage', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the getMessages operation.
     * @callback module:api/Class2MessagesApi~getMessagesCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Messages} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get a list of messages.
     * To receive only new messages, pass the **lastMessageNumber** parameter from the last query.  Files from messages are guaranteed to be stored only for 30 days and can be deleted. Download the files as soon as you get to your server.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.lastMessageNumber The lastMessageNumber parameter from the last response
     * @param {Boolean} opts.last Displays the last 100 messages. If this parameter is passed, then lastMessageNumber is ignored. (default to false)
     * @param {String} opts.chatId Filter messages by chatId  Chat ID from the message list. Examples: 17633123456@c.us for private messages and 17680561234-1479621234@g.us for the group.
     * @param {Number} opts.limit Sets length of the message list. Default 100. With value 0 returns all messages.
     * @param {Number} opts.minTime Filter messages received after specified time. Example: 946684800.
     * @param {Number} opts.maxTime Filter messages received before specified time. Example: 946684800.
     * @param {module:api/Class2MessagesApi~getMessagesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Messages}
     */
    this.getMessages = function(opts, callback) {
      opts = opts || {};
      var postBody = null;

      var pathParams = {
      };
      var queryParams = {
        'lastMessageNumber': opts['lastMessageNumber'],
        'last': opts['last'],
        'chatId': opts['chatId'],
        'limit': opts['limit'],
        'min_time': opts['minTime'],
        'max_time': opts['maxTime'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Messages;
      return this.apiClient.callApi(
        '/messages', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendContact operation.
     * @callback module:api/Class2MessagesApi~sendContactCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Sending a contact or contact list to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendContactRequest} sendContactRequest 
     * @param {module:api/Class2MessagesApi~sendContactCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendContact = function(sendContactRequest, callback) {
      var postBody = sendContactRequest;
      // verify the required parameter 'sendContactRequest' is set
      if (sendContactRequest === undefined || sendContactRequest === null) {
        throw new Error("Missing the required parameter 'sendContactRequest' when calling sendContact");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendContact', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendFile operation.
     * @callback module:api/Class2MessagesApi~sendFileCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Send a file to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendFileRequest} sendFileRequest 
     * @param {module:api/Class2MessagesApi~sendFileCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendFile = function(sendFileRequest, callback) {
      var postBody = sendFileRequest;
      // verify the required parameter 'sendFileRequest' is set
      if (sendFileRequest === undefined || sendFileRequest === null) {
        throw new Error("Missing the required parameter 'sendFileRequest' when calling sendFile");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendFile', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendLink operation.
     * @callback module:api/Class2MessagesApi~sendLinkCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Send text with link and link's preview to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendLinkRequest} sendLinkRequest 
     * @param {module:api/Class2MessagesApi~sendLinkCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendLink = function(sendLinkRequest, callback) {
      var postBody = sendLinkRequest;
      // verify the required parameter 'sendLinkRequest' is set
      if (sendLinkRequest === undefined || sendLinkRequest === null) {
        throw new Error("Missing the required parameter 'sendLinkRequest' when calling sendLink");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendLink', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendLocation operation.
     * @callback module:api/Class2MessagesApi~sendLocationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Sending a location to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendLocationRequest} sendLocationRequest 
     * @param {module:api/Class2MessagesApi~sendLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendLocation = function(sendLocationRequest, callback) {
      var postBody = sendLocationRequest;
      // verify the required parameter 'sendLocationRequest' is set
      if (sendLocationRequest === undefined || sendLocationRequest === null) {
        throw new Error("Missing the required parameter 'sendLocationRequest' when calling sendLocation");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendLocation', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendMessage operation.
     * @callback module:api/Class2MessagesApi~sendMessageCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Send a message to a new or existing chat.
     * The message will be added to the queue for sending and delivered even if the phone is disconnected from the Internet or authorization is not passed.  Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendMessageRequest} sendMessageRequest 
     * @param {module:api/Class2MessagesApi~sendMessageCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendMessage = function(sendMessageRequest, callback) {
      var postBody = sendMessageRequest;
      // verify the required parameter 'sendMessageRequest' is set
      if (sendMessageRequest === undefined || sendMessageRequest === null) {
        throw new Error("Missing the required parameter 'sendMessageRequest' when calling sendMessage");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendMessage', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendPTT operation.
     * @callback module:api/Class2MessagesApi~sendPTTCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Send a ptt-audio to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendPTTRequest} sendPTTRequest 
     * @param {module:api/Class2MessagesApi~sendPTTCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendPTT = function(sendPTTRequest, callback) {
      var postBody = sendPTTRequest;
      // verify the required parameter 'sendPTTRequest' is set
      if (sendPTTRequest === undefined || sendPTTRequest === null) {
        throw new Error("Missing the required parameter 'sendPTTRequest' when calling sendPTT");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendPTT', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the sendVCard operation.
     * @callback module:api/Class2MessagesApi~sendVCardCallback
     * @param {String} error Error message, if any.
     * @param {module:model/SendMessageStatus} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Sending a vcard to a new or existing chat.
     * Only one of two parameters is needed to determine the destination - chatId or phone.
     * @param {module:model/SendVCardRequest} sendVCardRequest 
     * @param {module:api/Class2MessagesApi~sendVCardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/SendMessageStatus}
     */
    this.sendVCard = function(sendVCardRequest, callback) {
      var postBody = sendVCardRequest;
      // verify the required parameter 'sendVCardRequest' is set
      if (sendVCardRequest === undefined || sendVCardRequest === null) {
        throw new Error("Missing the required parameter 'sendVCardRequest' when calling sendVCard");
      }

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['instanceId', 'token'];
      var contentTypes = ['application/x-www-form-urlencoded', 'application/json'];
      var accepts = ['application/json'];
      var returnType = SendMessageStatus;
      return this.apiClient.callApi(
        '/sendVCard', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }
  };

  return exports;
}));

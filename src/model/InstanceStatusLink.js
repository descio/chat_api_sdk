/**
 * Chat API SDK
 * The SDK allows you to receive and send messages through your WhatsApp account. [Sign up now](https://app.chat-api.com/)  The Chat API is based on the WhatsApp WEB protocol and excludes the ban both when using libraries from mgp25 and the like. Despite this, your account can be banned by anti-spam system WhatsApp after several clicking the \"block\" button.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: sale@chat-api.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.4.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.ChatApiSdk) {
      root.ChatApiSdk = {};
    }
    root.ChatApiSdk.InstanceStatusLink = factory(root.ChatApiSdk.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The InstanceStatusLink model module.
   * @module model/InstanceStatusLink
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>InstanceStatusLink</code>.
   * Status information link
   * @alias module:model/InstanceStatusLink
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>InstanceStatusLink</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/InstanceStatusLink} obj Optional instance to populate.
   * @return {module:model/InstanceStatusLink} The populated <code>InstanceStatusLink</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('label')) {
        obj['label'] = ApiClient.convertToType(data['label'], 'String');
      }
      if (data.hasOwnProperty('link')) {
        obj['link'] = ApiClient.convertToType(data['link'], 'String');
      }
    }
    return obj;
  }

  /**
   * Link caption for the button
   * @member {String} label
   */
  exports.prototype['label'] = undefined;
  /**
   * Reference URL instead of method
   * @member {String} link
   */
  exports.prototype['link'] = undefined;



  return exports;
}));


